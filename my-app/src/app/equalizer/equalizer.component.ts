import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';

@Component({
  selector: 'app-equalizer',
  templateUrl: './equalizer.component.html',
  styleUrls: ['./equalizer.component.css']
})
export class EqualizerComponent implements OnInit {
  buttonImage = 'url(/assets/button_gac_on.png)'
  @Input() panel : string;
  @Output() onClick = new EventEmitter()
  oneButton = 'url(/assets/button_gac_on.png)'
  twoButton = 'url(/assets/button_gac_on.png)'

changePanel (value: string) {

    this.onClick.emit(value);
  console.log(value);

}
  constructor() {
  this.panel = "arp_panel"
  }

  ngOnInit(): void {
  }

}
