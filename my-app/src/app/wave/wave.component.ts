import { Component, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';

@Component({
  selector: 'app-wave',
  templateUrl: './wave.component.html',
  styleUrls: ['./wave.component.css']
})
export class WaveComponent implements OnInit {

  @ViewChild('canvasEl', { read: ElementRef }) WaveCanvas!: ElementRef;
 /////////////////////////////////////////////
  width  = 40
  cellsHeights = [90,40,50,70,50]

  soundSteps =[1, 3, 1, 2, 1]
  cellPositions = [1, 4, 5, 7, 8]
  plusArray = ["+","+","+","+","+"]
  minusArray = ["-","-","-","-","-"]
  AttackDecayCurves=[]
  color ='M 2 120 S 0 70, 10 2 C 15 2, 19 70, 17 120 '
  cellColors= ["#FFD700","transparent","transparent","transparent","transparent"]
  steps=8
  currentStep=0

cellValueChange (event:any, k:number){
  console.log(this.cellsHeights)
  console.log(k)
  this.cellsHeights [k]= event.target.value
}
stepConfigChange(event:any){
   this.steps = this.soundSteps.reduce((previous,current, index) => previous + current, 0)
    console.log(this.soundSteps)
    while(Number(event.target.value) != this.steps){
      if ( event.target.value >this.steps ){
       this.soundSteps.push(1)
       this.cellsHeights.push(50)
        this.cellColors.push("transparent")
        this.plusArray.push("+")
        this.minusArray.push("-")
       this.steps++
     }
     else if (event.target.value < this.steps){
       this.steps=this.steps-this.soundSteps.pop()!
       this.cellsHeights.pop()
        this.cellColors.pop()
        this.plusArray.pop()
        this.minusArray.pop()
       console.log(this.soundSteps)
     }
    }
  this.soundSteps.reduce((previousValue,currentValue, index) =>
  {this.cellPositions[index]=previousValue + currentValue; return previousValue + currentValue}, 0)
    console.log(this.steps)
  }
  addCell(index:number){
    this.soundSteps[index]--
    this.soundSteps.splice(index+1, 0, 1)
    this.cellsHeights.splice(index+1, 0, 50)
    this.cellColors.push("transparent")
    this.plusArray.push("+")
    this.minusArray.push("-")
    this.soundSteps.reduce((previousValue,currentValue, index) =>
    {this.cellPositions[index]=previousValue + currentValue; return previousValue + currentValue}, 0)
    console.log(this.soundSteps)
    console.log(this.cellPositions)
  }
  removeCell(index:number){
    this.soundSteps[index]=this.soundSteps[index]+this.soundSteps[index+1]
    this.soundSteps.splice(index+1, 1)
    this.cellsHeights.splice(index+1, 1)
    this.soundSteps.reduce((previousValue,currentValue, index) =>
    {this.cellPositions[index]=previousValue + currentValue; return previousValue + currentValue}, 0)

  }
  changeCurrentCell (current:number ){
    this.cellColors[this.currentStep]="transparent"
    this.currentStep=current
    this.cellColors[this.currentStep]="#FFD700"
  }
/////////////////////////////////////////////



  constructor() { }
  x=0
  y=0
  cx = 0
  cy =0
  i = 0
draw (){
  console.log(this.WaveCanvas)
  const ctx = this.WaveCanvas.nativeElement.getContext('2d')
  console.log(ctx)
  if (!ctx) return
  ctx.fillStyle = "black"
  ctx.strokeStyle = "olive";
  ctx.beginPath();
  this.cy = this.WaveCanvas.nativeElement.height / 2
  ctx.moveTo(this.cx, this.cy);
  for (this.i = 1; this.i<200; this.i++) {
    this.x = this.i*3;
    this.y = 40*Math.sin(10*this.i/180*Math.PI);
    ctx.lineTo(this.cx+this.x, this.cy+this.y);
  }
  ctx.lineWidth = 4;
  this.cx = this.cx+5
  ctx.stroke()
}



  ngOnInit(): void {}

}
