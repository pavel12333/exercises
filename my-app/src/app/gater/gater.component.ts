import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gater',
  templateUrl: './gater.component.html',
  styleUrls: ['./gater.component.css']
})
export class GaterComponent implements OnInit {
  soundSteps =[1, 1, 1, 1]
  plusArray = ["","+","+","+"]
  AttackDecayCurves=[]
  color ='M 2 120 S 0 70, 10 2 C 15 2, 19 70, 17 120 '
  cellsHeights = [120,40,50,70]
  cellColors= ["#FFD700","transparent","transparent","transparent"]
  steps=0
  currentStep=0
  valueChange (event:any){
    console.log(event.target.value)
    this.cellsHeights [this.currentStep]= event.target.value
  }
  stepChange(event:any){
    this.steps = this.soundSteps.reduce((a, b) => a + b, 0)
    console.log(this.steps)
    while(Number(event.target.value) != this.steps){
      if ( event.target.value >this.steps ){
        this.soundSteps.push(1)
        this.cellsHeights.push(50)
        this.cellColors.push("transparent")
        this.plusArray.push('+')
        this.steps++
        console.log(this.soundSteps)
      }
      else if (event.target.value < this.steps){
        this.steps=this.steps-this.soundSteps.pop()!
        this.cellsHeights.pop()
        this.cellColors.pop()
        this.plusArray.pop()
        console.log(this.soundSteps)
      }
    }
    console.log(this.steps)
  }
  addMode(){
    console.log("add node")
  }
  changeCurrentCell (current:number){
    this.cellColors[this.currentStep]="transparent"
    this.currentStep=current
    this.cellColors[this.currentStep]="#FFD700"
  }



  constructor() { }

  ngOnInit(): void {
  }

}
