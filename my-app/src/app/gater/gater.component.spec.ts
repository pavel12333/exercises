import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaterComponent } from './gater.component';

describe('GaterComponent', () => {
  let component: GaterComponent;
  let fixture: ComponentFixture<GaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
