import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EqualizerComponent } from './equalizer/equalizer.component';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {DragDropModule} from '@angular/cdk/drag-drop';
import { WaveComponent } from './wave/wave.component';
import { KnobComponent } from './knob/knob.component';
import { GaterComponent } from './gater/gater.component';

@NgModule({
  declarations: [
    AppComponent,
    EqualizerComponent,
    WaveComponent,
    KnobComponent,
    GaterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSliderModule,
    BrowserAnimationsModule,
    MatButtonToggleModule,
    DragDropModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}
