import { Component, EventEmitter,Output, ElementRef, OnInit } from '@angular/core';
import { fromEvent, Subject, switchMap, tap, takeUntil, map, race, pairwise, finalize } from 'rxjs'

@Component({
  selector: 'app-knob',
  templateUrl: './knob.component.html',
  styleUrls: ['./knob.component.css']
})
export class KnobComponent implements OnInit {
  @Output() valueChange = new EventEmitter<number>()

  mouseDown$ = fromEvent<MouseEvent>(this.el.nativeElement, 'mousedown').pipe(
    tap(evt => evt.preventDefault()),
    map(evt => this.mapMouseEvent(evt)),
  )
  private mapMouseEvent(evt: MouseEvent) {
    console.log(evt.pageX)
    return { x: evt.pageX, y: evt.pageY }
  }
  constructor(private el: ElementRef) { }

  ngOnInit(): void {
  }

}
